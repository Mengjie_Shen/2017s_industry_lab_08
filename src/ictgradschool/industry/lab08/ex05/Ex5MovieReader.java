package ictgradschool.industry.lab08.ex05;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.*;
import java.util.Scanner;

/**
 * Created by mshe666 on 28/11/2017.
 */
public class Ex5MovieReader extends MovieReader{
    public void start() {


        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Load the movie data
        Movie[] films = loadMovies(fileName);
        // Do some stuff with the data to check that its working
        printMoviesArray(films);
        Movie mostRecentMovie = getMostRecentMovie(films);
        Movie longestMovie = getLongestMovie(films);
        printResults(mostRecentMovie, longestMovie);
        System.out.println();
        printDirector("Searching for Sugar Man", films);
        printDirector("Liberal Arts", films);
        printDirector("The Intouchables", films);


    }
    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        Movie[] movies = new Movie[1];
        try {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream inSer = new ObjectInputStream(fileIn);
/*            for (int i = 0; i < 19; i++) {
                Movie movie = (Movie)inSer.readObject();
                movies[i] = movie;
                System.out.println(movie.toString());
            }*/
            movies = (Movie[])inSer.readObject();

        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }catch (ClassNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }


        return movies;
    }

    public static void main(String[] args) {
        new Ex5MovieReader().start();
    }
}
