package ictgradschool.industry.lab08.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.*;

/**
 * Created by mshe666 on 28/11/2017.
 */
public class Ex5MovieWriter extends MovieWriter{
    public void start() {

        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Create and fill Movies array
        Movie[] films = getMovieData();

        // Saves the movies
        saveMovies(fileName, films);
    }

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        int counter = films.length;

        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream outSer = new ObjectOutputStream(fileOut);

            outSer.writeObject(films);
            outSer.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in movie.ser");

/*            for (int i = 0; i < counter; i++) {
//                System.out.println(films[i].toString());
                Movie movie = films[i];
//                System.out.println(movie.toString());

            }*/


        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

    }


    public static void main(String[] args) {
        new Ex5MovieWriter().start();
    }
}
