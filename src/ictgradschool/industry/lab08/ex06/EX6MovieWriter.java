package ictgradschool.industry.lab08.ex06;


import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;
import com.google.gson.Gson;

import java.io.*;

/**
 * Created by mshe666 on 28/11/2017.
 */
public class EX6MovieWriter extends MovieWriter{
    public void start() {

        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Create and fill Movies array
        Movie[] films = getMovieData();

        // Saves the movies
        saveMovies(fileName, films);
    }

    @Override
    protected void saveMovies(String fileName, Movie[] films) {
        File myFile = new File(fileName);
        // TODO Implement this with a PrintWriter
        try (BufferedWriter outPut = new BufferedWriter(new FileWriter(myFile))){

            Gson gson = new Gson();
            String json = gson.toJson(films);
            System.out.println("Data = " + json);
            outPut.write(json);

        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    public static void main(String[] args) {
        new EX6MovieWriter().start();
    }

}
