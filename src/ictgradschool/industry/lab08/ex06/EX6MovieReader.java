package ictgradschool.industry.lab08.ex06;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;
import com.google.gson.Gson;

import java.io.*;


/**
 * Created by mshe666 on 28/11/2017.
 */
public class EX6MovieReader extends MovieReader{
    public void start() {


        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Load the movie data
        Movie[] films = loadMovies(fileName);
        // Do some stuff with the data to check that its working
        printMoviesArray(films);
        Movie mostRecentMovie = getMostRecentMovie(films);
        Movie longestMovie = getLongestMovie(films);
        printResults(mostRecentMovie, longestMovie);
        System.out.println();
        printDirector("Searching for Sugar Man", films);
        printDirector("Liberal Arts", films);
        printDirector("The Intouchables", films);


    }
    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        File myFile = new File(fileName);
        Movie[] movies = new Movie[1];
        Gson gson = new Gson();
        try (BufferedReader inPut = new BufferedReader(new FileReader(myFile))){

            String jsonInString = inPut.readLine();
            movies = gson.fromJson(jsonInString, Movie[].class);

        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


        return movies;
    }

    public static void main(String[] args) {
        new EX6MovieReader().start();
    }
}
