package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File myFile = new File(fileName);

        try (Scanner inFile = new Scanner(new FileReader(myFile))) {
            String line = null;
            while (inFile.hasNextLine()) {
                System.out.println(inFile.nextLine());
            }
        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
