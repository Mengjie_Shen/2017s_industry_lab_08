package ictgradschool.industry.lab08.ex04;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.*;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    public void start() {


        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Load the movie data
        Movie[] films = loadMovies(fileName);

        // Do some stuff with the data to check that its working
        printMoviesArray(films);
        Movie mostRecentMovie = getMostRecentMovie(films);
        Movie longestMovie = getLongestMovie(films);
        printResults(mostRecentMovie, longestMovie);
        System.out.println();
        printDirector("Searching for Sugar Man", films);
        printDirector("Liberal Arts", films);
        printDirector("The Intouchables", films);

    }

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        Movie[] movies = new Movie[1];
        File myFile = new File(fileName);
        try (Scanner csvInput = new Scanner(myFile)) {
            csvInput.useDelimiter(",|\\n");
//            System.out.println(csvInput.next());
            int count = csvInput.nextInt();
//            System.out.println(count);
            movies = new Movie[count];
            for (int j = 0; j < movies.length; j++) {
                int num = csvInput.nextInt();
                String name = csvInput.next();
                int year = csvInput.nextInt();
                int minutes = csvInput.nextInt();
                String director = csvInput.next();
                movies[j] = new Movie(name, year, minutes, director);
//                System.out.println(movies[i].toString());
            }
        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return movies;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
