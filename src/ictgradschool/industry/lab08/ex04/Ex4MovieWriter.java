package ictgradschool.industry.lab08.ex04;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {
    public void start() {

        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Create and fill Movies array
        Movie[] films = getMovieData();

        // Saves the movies
        saveMovies(fileName, films);
    }

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        File myFile = new File(fileName);
        int counter = films.length;
            try (PrintWriter csvOutput = new PrintWriter(myFile)) {
                csvOutput.write(counter + "\n");
                for (int i = 0; i < films.length; i++) {
                    csvOutput.write(i + "," + films[i].getName() + "," + films[i].getYear() + "," + films[i].getLengthInMinutes() + "," + films[i].getDirector() + "\n");
                }

        }catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

    }



    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
