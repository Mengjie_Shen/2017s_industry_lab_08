package ictgradschool.industry.lab08.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        File myFile = new File("input2.txt");
        try (FileReader fR = new FileReader(myFile)){
            int num = fR.read();
            while (num != -1) {
                total++;
                if (num == 'e' || num == 'E') {
                    numE++;
                }
                num = fR.read();
            }
            fR.close();
        } catch(IOException e) {
            System.out.println("IO problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        File myFile = new File("input2.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader(myFile))) {
            int num = reader.read();
            while (num != -1) {
                total++;
                if (num == 'e' || num == 'E') {
                    numE++;
                }
                num = reader.read();
            }
        }catch (IOException e) {
            System.out.println("IO problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
