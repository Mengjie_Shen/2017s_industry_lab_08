package ictgradschool.industry.lab08.ex03;
import java.io.*;

public class Movie implements java.io.Serializable {
    
    private String name;
    private int year;
    private int lengthInMinutes;
    private String director;
    
    public Movie(String name, int year, int lengthInMinutes, String director) {
        
        this.name = name;
        this.year = year;
        this.lengthInMinutes = lengthInMinutes;
        this.director = director;
    }
    
    public String toString() {
        return name + " (" + year + "), "
            + lengthInMinutes + " minutes, Director: " + director;
    }
    
    public boolean isOlderThan(Movie other) {
        return year < other.year;
    }
    
    public boolean isMoreRecentThan(Movie other) {
        return year > other.year;
    }
    
    public boolean isShorterThan(Movie other) {
        return lengthInMinutes < other.lengthInMinutes;
    }
    
    public boolean isLongerThan(Movie other) {
        return lengthInMinutes > other.lengthInMinutes;
    }
    
    public boolean equals(Movie other) {
        return name.equals(other.name)
            && year == other.year
            && lengthInMinutes == other.lengthInMinutes
            && director.equals(other.director);
    }
    
    public String getName() {
        return name;
    }
    
    public int getYear() {
        return year;
    }
    
    public int getLengthInMinutes() {
        return lengthInMinutes;
    }
    
    public String getDirector() {
        return director;
    }

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        stream.writeObject(name);
        stream.writeInt(year);
        stream.writeInt(lengthInMinutes);
        stream.writeObject(director);
    }

    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        name = (String) stream.readObject();
        year = stream.readInt();
        lengthInMinutes = stream.readInt();
        director = (String) stream.readObject();
    }



}